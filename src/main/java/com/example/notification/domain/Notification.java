package com.example.notification.domain;

import com.example.notification.enumeration.EmailTemplate;
import com.example.notification.enumeration.Roles;
import lombok.Data;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Set;

/**
 * @author maxima - 4/17/19
 */
@Data
public class Notification {
    private UserDto sender;
    private List<UserDto> userDtoList;
    private String content;
    private EmailTemplate template;
    private Set<Roles> roles;
    private ZonedDateTime create;
}
