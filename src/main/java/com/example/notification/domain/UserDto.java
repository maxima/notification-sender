package com.example.notification.domain;

import lombok.Data;

/**
 * @author maxima - 4/16/19
 */
@Data
public class UserDto {
    private int id;
    private String name;
    private String email;
}
