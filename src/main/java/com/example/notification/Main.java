package com.example.notification;

import com.example.notification.decorator.*;
import com.example.notification.domain.Notification;
import com.example.notification.enumeration.EmailTemplate;

/**
 * @author maxima - 4/16/19
 */

public class Main {


    public static void main(String[] args) {

        Notification notification = new Notification();

        notification.setContent("test content");
        notification.setTemplate(EmailTemplate.INVENTORY);

        Notificator notificator = new EmailDecorator(new PushDecorator(new SmsDecorator(new NotificationComponent())));

        notificator.send(notification);


    }
}
