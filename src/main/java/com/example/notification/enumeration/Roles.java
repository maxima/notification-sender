package com.example.notification.enumeration;

/**
 * @author maxima - 4/17/19
 */

public enum Roles {
    USER,
    ADMIN
}
