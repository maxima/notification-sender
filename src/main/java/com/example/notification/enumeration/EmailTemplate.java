package com.example.notification.enumeration;

/**
 * @author maxima - 4/16/19
 */

public enum EmailTemplate {
    
    INVENTORY("inventory_template"),
    GAS_REQUEST("gas_reqest_template"),
    NEWS("news_template");

    private String template;

    EmailTemplate(String template) {
        this.template = template;
    }

    public String getTemplate(){
        return template;
    }
}
