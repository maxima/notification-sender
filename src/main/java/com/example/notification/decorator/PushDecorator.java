package com.example.notification.decorator;

import com.example.notification.domain.Notification;

/**
 * @author maxima - 4/16/19
 */

public class PushDecorator extends NotificationDecorator {

    PushDecorator(Notificator notificator) {
        super(notificator);
    }

    @Override
    public void send(Notification notification) {
        System.out.println(".... start send push notification - " + notification);

        super.send(notification);
    }

}
