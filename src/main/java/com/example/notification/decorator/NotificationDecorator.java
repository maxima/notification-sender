package com.example.notification.decorator;

import com.example.notification.domain.Notification;

/**
 * @author maxima - 4/16/19
 */

public abstract class NotificationDecorator implements Notificator {

    protected Notificator notificator;

    public NotificationDecorator(Notificator notificator) {
        this.notificator = notificator;
    }

    @Override
    public void send(Notification notification) {
        notificator.send(notification);
    }
}
