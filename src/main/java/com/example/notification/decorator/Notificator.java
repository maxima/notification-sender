package com.example.notification.decorator;

import com.example.notification.domain.Notification;
import com.example.notification.domain.UserDto;
import com.example.notification.enumeration.EmailTemplate;
import com.example.notification.enumeration.Roles;

import java.util.List;

/**
 * @author maxima - 4/15/19
 */

public interface Notificator {
    void send(Notification notification);
}
