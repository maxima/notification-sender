package com.example.notification.decorator;

import com.example.notification.domain.Notification;

/**
 * @author maxima - 4/16/19
 */

public class SmsDecorator extends NotificationDecorator {

    SmsDecorator(Notificator notificator) {
        super(notificator);
    }

    @Override
    public void send(Notification notification) {
        System.out.println(".... start send sms notification - " + notification);
        super.send(notification);
    }
}
